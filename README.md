NAME
====

App::RunForPid - Given a X Window, attach a process for debug (gdb, tail, etc)

SYNOPSIS
========

```sh
run-for-pid -h
Usage:
  ./bin/run-for-pid -- Find a pid for a window
  ./bin/run-for-pid <pid> -- Execute a command for a child process of $pid
```

DESCRIPTION
===========

App::RunForPid is a program to attach different tools to a given
running process.

The idea is to quickly attach or get information about that process
only from the window that's running it. There's a fallback for
background process with the `pid` parameter.

DEPENDENCIES
============

* `xprop` — to get properties for a X Window
* `pstree`

OPTIONAL DEPENDENCIES
---------------------

* `gdb` — Attach a gdb to a given process
* `strace` — Read stdout and stderr of a given process
* `tail` — Keep logging a file

AUTHOR
======

Matias Linares <matiaslina@deprecated.org>

COPYRIGHT AND LICENSE
=====================

Copyright 2020 Matias Linares

This library is free software; you can redistribute it and/or modify it under the Artistic License 2.0.
